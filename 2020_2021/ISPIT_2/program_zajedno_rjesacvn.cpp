//Omoguciti korisniku unos dimenzija
//matrice (2-D polja) m�n, gdje je 17 < m <= 30
//i 7 <= n <= 12.
#include<stdio.h>
#include<time.h>
#include<stdlib.h>
int main(){
	srand((unsigned)time(NULL));
	float polje[30][12];
	int m,n;

	
	do{	
		printf("Unesite m: ");
		scanf("%d", &m);
	}while(m<=17 || m>30);
	//}while((m<17 && m<=30)==0);  //isto funkcionira
	
	do{
	printf("Unesite n: ");
	scanf("%d", &n);
	
	}while(n<7 || n>12);
	//}while((7<=n && n<=12)==0);
	
	//dimenzije dobre
	
	for(int i=0;i<m;i++){
		for(int j=0;j<n;j++){
			polje[i][j]=((float)rand()/RAND_MAX)*45+5;
		}
	}
	
	
	
	float max=polje[0][0];
	int max_redak=0;
	int max_stupac=0;
	
	for(int i=0;i<m;i++){
		for(int j=0;j<n;j++){
			if(polje[i][j]>max){
				max=polje[i][j];
				max_redak=i;
				max_stupac=j;
			}
		}
	}
	
	printf("\n\nMaks= %.2f, nalazi se u redku %d, stupcu %d \n\n", max, max_redak, max_stupac);
	
	for(int i=0;i<n;i++){
		if(i != max_stupac){
			polje[max_redak][i]=1;
		}
	}
	
	for(int i=0;i<m;i++){
		if(i != max_redak){
			polje[i][max_stupac]=-1;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//ispis
	printf("\n\n");
	for(int i=0;i<m;i++){
		for(int j=0;j<n;j++){
			printf("%.2f  ", polje[i][j]);
		}
		printf("\n");
	}
	
	
	
	return 0; // ovo sam zaboravio napisati tijekom lekcije
	
}
