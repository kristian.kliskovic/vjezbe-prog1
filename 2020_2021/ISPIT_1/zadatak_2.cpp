#include<stdio.h>

int faktorijel(int n){
	int i;
	int rezultat=1;
	for(i=1;i<=n;i++){
		rezultat*=i;
	}
	return rezultat;
}

int broj_znamenki_faktorijela(int n){
	int br_znam=1;
	n=faktorijel(n);
	while(n>9){
		n/=10;
		br_znam++;
	}
	return br_znam;
}

int main(){
	int broj=11;
	printf("Broj znamenki je %d.", broj_znamenki_faktorijela(broj));
	
	return 0;
}
