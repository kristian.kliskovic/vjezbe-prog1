#include<math.h>
#include<stdio.h>

float funkcija(float *polje, int n){
    float max=polje[0];
    float sredina=0;
    int i;
    for(i=0;i<n;i++){
        if(polje[i]>max) max=polje[i];
        
        sredina+=polje[i];
    }
    sredina/=n;

    return fabs(sredina-max);//fabs je | |, absoultna vrijednost za realne brojeve, potreban math.h
}



//testiranje koda nije potrebno
int main(){
    float polje[13]={0.1, 0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1, 100,-99.8};
    printf("%.2f", funkcija(polje,13));
}