//Omogu�iti korisniku unos dimenzija matrice (2-D polja) m�n, gdje su 10 ? m < 19
//i 11 ? n ? 20. Popuniti matricu pseudo-slu�ajnim brojevima iz [-10, 20] e R. Odrediti
//najve�u vrijednost u svakom stupcu i izra�unati njihov zbroj te ga potom ispisati na ekran
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

void ispis(float polje[18][20], int red, int stupac){
	for(int i=0;i<red;i++){
		for(int j=0;j<stupac;j++){
			printf("%f  ", polje[i][j]);
		}
		printf("\n");
	}
}


int main(){
	srand((unsigned)time(NULL));
	float polje[18][20];
	int red, stupac;
	do{
		printf("Unesite broj redaka: ");
		scanf("%d", &red);
	}while(red<5||red>18);
	do{
		printf("Unesite broj stupaca: ");
		scanf("%d", &stupac);
	}while(stupac<5||stupac>20);
	int i,j;
	for(i=0;i<red;i++)
		for(j=0;j<stupac;j++)
			polje[i][j]=(float)rand()/RAND_MAX*(20-(-10))+(-10);
	float zbroj=0, max_stupac;
	for(j=0;j<stupac;j++){
		max_stupac=-10;
		for(i=0;i<red;i++)
			if(polje[i][j]>max_stupac){
				max_stupac=polje[i][j];
			}
		zbroj+=max_stupac;
	}
	printf("\n\n%.1f\n\n", zbroj);
	ispis(polje,red,stupac);	
}
