#include <stdio.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>
float funkcija(float x[], int n){
	float zbroj=0;
	for(int i=0;i<(n-1);i++){
		for(int j=i+1;j<n;j++){
			zbroj+=fabs(x[i]-x[j]);
		}
	}
	return zbroj;
}

int main (void) {
	srand((unsigned)(time(NULL)));
	float polje[50];
	for(int i=0;i<50;i++){
		polje[i]=1.0*rand()/RAND_MAX*100;
	}
	printf("%f", funkcija(polje, 50));	
}
