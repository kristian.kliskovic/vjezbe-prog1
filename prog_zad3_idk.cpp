#include<stdio.h>
#include<stdlib.h>
int parnost(int broj){
	if(broj<0) broj*=-1;
	do{
		if(broj%2==1) return 0; //ako ostatak dijeljenja sa 2 jednak 1 onda sigurno nisu SVE znamenke parne, jer smo pokazali da bas ta nije
		broj/=10;
	}while(broj>0);
	return 1;
}

float udio_parnih(int *polje, int n){
	int zbroj=0;
	int i;
	for(i=0;i<n;i++){
		if(parnost(polje[i])==1) zbroj++;
	}
	return (float)zbroj/n*100;
}

int main(){
	int *polje=(int*)calloc(30, sizeof(short int));
	for(int i=0;i<30;i++)
		scanf("%d", &polje[i]);
	printf("%.2f", udio_parnih(polje,30));
	return 0;
}
