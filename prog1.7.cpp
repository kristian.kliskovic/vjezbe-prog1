#include<stdio.h>
int faktorijel(int n){
	int ret=1;
	for(int i=2;i<=n;i++){
		ret*=i;
	}
	return ret;
}
int zbroj_znam_faktorijel(int n){
	if(n<0) return -1;
	n=faktorijel(n);
	int zbroj=0;
	do{
		zbroj+=n%10;
		n/=10;
	}while(n!=0);
	return zbroj;
}

int main(void){
	printf("%d", zbroj_znam_faktorijel(11));
	return zbroj_znam_faktorijel(11);
}
