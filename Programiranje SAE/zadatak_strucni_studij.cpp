#include<time.h>
#include<stdlib.h>
#include<stdio.h>

void funkcija(char *X){
	for(int i=0;X[i]!='\0';i++){
		if(X[i]>='A'&&X[i]<='Z'){
			X[i]+=32; // 32 je razlika u zapisu malih i velikih slova, recimo 'A' je 65, a 'a' je 97
		}
	}
}

int main(){
	char *pok;
	pok=(char*)calloc(100, sizeof(char));
	fgets(pok,99,stdin); //maksimalni broj znakova za unos je 99 zbog null znaka na kraju
	//printf("%s\n", pok);
	funkcija(pok);
	printf("%s\n", pok);
	free(pok);
}

