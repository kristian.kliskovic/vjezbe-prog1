#include<stdio.h>
#include<stdlib.h>
#include<time.h>

float funkcija(float *p1, float *p2, int n1, int n2){
	float prosjek1=0, prosjek2=0;
	for(int i=1;i<n1;i+=2){
		prosjek1+=p1[i];
	}
	for(int i=1;i<n2;i+=2){
		prosjek2+=p2[i];
	}
	prosjek1/=n1;
	prosjek2/=n2;
	if(prosjek1>prosjek2) return prosjek2;
	else return prosjek1;
}
int main(){
	srand((unsigned)time(NULL));
	float polje1[25];
	float polje2[50];
	for(int i=0;i<25;i++){
		polje1[i]=(float)rand()/RAND_MAX*40-10;
	}
	for(int i=0;i<50;i++){
		polje2[i]=(float)rand()/RAND_MAX*40-10;
	}
	printf("%.3f", funkcija(polje1, polje2, 25,50));
	return 0;
}
