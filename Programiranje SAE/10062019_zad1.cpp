#include<stdio.h>
int prost(int n){
	printf("%d ", n);
	for(int i=2;i<=n/2+1;i++){
		if(n%i==0) return 0;
	}
	return 1;
}
int funkcija(int n){
	int brojac=0;
	do{
		brojac+=prost(n%10);
		n/=10;
	}while(n!=0);
	return brojac;
}
int main(){
	printf("%d", funkcija(1245));
	return 0;
}
