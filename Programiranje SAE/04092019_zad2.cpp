#include<stdio.h>
#include<stdlib.h>

int funkcija(int *polje, int n){
	int brojac=0;
	int temp;
	for(int i=0;i<n;i++){
		temp=polje[i];
		if(temp<0) temp*=-1;
		while(temp>=10) temp/=10;
		if(temp%3==0) brojac++;
	}
	return brojac;
}
int main(){
	int n;
	printf("Koliko elemenata: ");
	scanf("%d", &n);
	int *brojevi=(int*)calloc(n,sizeof(int));
	for(int i=0;i<n;i++){
		scanf("%d", &brojevi[i]);
	}
	printf("%d", funkcija(brojevi, n));
	return 0;
}
