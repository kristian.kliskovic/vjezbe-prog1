#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int funkcija(int *polje, int n){
	float vrijednost=0;
	int broj_parnih=0;
	int brojac;
	for(int i=0;i<n;i++){
		if(i%2==0){
			vrijednost+=polje[i];
			broj_parnih++;
		}
	}
	vrijednost/=broj_parnih;
	for(int i=0;i<n;i++){
		if(polje[i]>vrijednost) brojac++;
	}
	return brojac;
}
int main(){
	srand((unsigned)time(NULL));
	int polje[17];
	for(int i=0;i<17;i++){
		polje[i]=rand()%5-1;
	}
	printf("\n");
	for(int i=0;i<17;i++){
		printf("%d ", polje[i]);
	}
	printf("\n %d", funkcija(polje,17));
	return 0;
}
