#include<time.h>
#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#define pi 3.141592
double funk(double *polje, int n){
	double ret=0;
	for(int i=1;i<n-1;i++){
		ret+=abs(pow(polje[i-1],2)-pow(polje[i+1],2));
	}
	ret/=n;
	return ret;
}
int main(){
	srand((unsigned)time(NULL));
	double *X=(double*)calloc(15,sizeof(double));
	for(int i=0;i<15;i++){
		X[i]=(double)rand()/RAND_MAX*10*pi-5*pi;
	}
	printf("%.2lf",funk(X,15));
	free(X);
	return 0;
}
